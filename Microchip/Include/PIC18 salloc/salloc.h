
// Summary: A macro used to enable nead-model RAM addressing
// Description: By uncommenting the NEAR_MODEL macro, the user can enable near-model RAM addressing when using dynamic FSFILE object
//              allocation with PIC18
#define NEAR_MODEL

// Summary: A macro used to define the heap size for PIC18
// Description: When using dynamic FSFILE object allocation with PIC18, the MAX_HEAP_SIZE will allow the user to specify the size
//              of the dynamic heap to use
#define	MAX_HEAP_SIZE		0x100



#if defined(NEAR_MODEL)
// Summary: A macro used to specify the near-model action
// Description: Functions can be declared using the NEAR macro.  If the NEAR_MODEL macro is uncommented, the NEAR macro will be ignored.
#ifdef __CCI__
#define	NEAR	__near
#else
#define	NEAR	near
#endif
#else
#define NEAR
#endif

// Description: A macro used to determine the maximum size of a dynamic memory segment.
#define	_MAX_SEGMENT_SIZE	0x7F
// Description: A macro used to determine the heap initialization size.
#define _MAX_HEAP_SIZE 	MAX_HEAP_SIZE-1

unsigned char * NEAR SRAMalloc(unsigned char nBytes);
void SRAMfree(unsigned char * NEAR pSRAM);
void SRAMInitHeap(void);
